import ButtonGroupComponent from "./buttongroup";
import ChosenDates from "./chosenDates";
import CurrentDate from "./currentDate";
import { Paper, Stack } from "@mui/material";
import Divider from '@mui/material/Divider';
import { styles } from './styles';
import React, { useCallback, useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import scheduleService from '../../api/hooks/schedule';

const CALENDAR = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]

function createData(id: string, startDate: string, endDate: string) {
    return {
        id,
        startDate,
        endDate,
    };
}


export default function DatePicker() {
    const [days, setDays] = useState<any[]>(getDaysStyles());
    const [startDate, setStartDate] = useState(0);
    const [endDate, setEndDate] = useState(0);
    
    const [rows, setRows] = React.useState([]);
    const [isFirstRender, setFirstRender] = React.useState(true);

    function getDaysStyles() {
        var basicStyles: any[] = [];
        CALENDAR.forEach(day => {
            basicStyles.push(styles.emptyCell);
        });
        return basicStyles;
    }

    const handleCellClick = (day: number) => {
        var arr = [];
        if (endDate !== 0 || startDate === 0) {
            arr = getDaysStyles();
            arr[day - 1] = styles.cellCircle;
            setStartDate(day);
            setEndDate(0);
        } else {
            arr = days;
            if (startDate > day) {
                arr[day - 1] = styles.startCellCircle;
                for (let index = day; index < startDate - 1; index++) {
                    arr[index] = styles.betweenCellCircle;
                }
                arr[startDate - 1] = styles.endCellCircle;
                setStartDate(day);
                setEndDate(startDate);

            } else {
                arr[startDate - 1] = styles.startCellCircle;
                for (let index = startDate; index < day - 1; index++) {
                    arr[index] = styles.betweenCellCircle;
                }
                arr[day - 1] = styles.endCellCircle;
                setEndDate(day);
            }
        }
        setDays(arr);


    }

    const createSchedule = () => {
        const row = createData("1", startDate.toString() + ".11.2021", endDate.toString() + ".11.2021");
        let list: any = [...rows, row];
        setRows(list);

        const add = async () => {
            const resp = await scheduleService.createSchedule(startDate.toString(), endDate.toString());
            console.log(resp);
            if (resp !== 'Added') {
                list.pop();
                list = [...list];
                setRows(list);
            }
        }
        add();
    }

    const getSchedule = () => {
        const get = async () => {
            const data = await scheduleService.getSchedule();
            const newSchedule = data.map((s: any) => createData(s.id, s.start_date, s.end_date));
            setRows(newSchedule);
        }
        get();
    };



    useEffect(
        () => {
            if (startDate !== 0 && endDate !== 0) {
                createSchedule();
                setStartDate(0);
                setEndDate(0);
            }

            if (isFirstRender) {
                getSchedule();
                setFirstRender(false);
            }
        }
    );

    const tableHead = (
        <TableHead>
            <TableRow>
                <TableCell style={styles.emptyCell}>SU</TableCell>
                <TableCell style={styles.emptyCell}>MO</TableCell>
                <TableCell style={styles.emptyCell}>TU</TableCell>
                <TableCell style={styles.emptyCell}>WE</TableCell>
                <TableCell style={styles.emptyCell}>TH</TableCell>
                <TableCell style={styles.emptyCell}>FR</TableCell>
                <TableCell style={styles.emptyCell}>SA</TableCell>
            </TableRow>
        </TableHead>
    )

    function getBody() {
        const calendar = [];
        const preprocCal = CALENDAR.map(day => {
            return <TableCell style={days[day - 1]} onClick={() => handleCellClick(day)}><b>{day}</b></TableCell>
        });

        for (let index = 0; index < preprocCal.length / 7; index++) {
            calendar.push(<TableRow>{preprocCal.slice(index * 7, index * 7 + 7)}</TableRow>)
        }

        return (
            <TableBody>
                {calendar}
            </TableBody>
        );
    }
    return (
        <div style={{ display: 'flex', alignItems: 'center' }}>
            <Stack style={styles.calendarClass}>
                <Paper elevation={0}>
                    <ChosenDates
                        startDate="27 AUG 2018"
                        endDate="31 AUG 2018"
                        background="white"
                        width="220px"
                        height="70px"
                        marginBottom="5px"
                        leftDateColor="#ae94e3"
                        rightDateColor="#40a2db"
                        fontSize="20px"
                        radius="10px"
                        textAlign="left"
                    >
                    </ChosenDates>
                </Paper>
                <Paper elevation={0}>
                    <CurrentDate />
                </Paper>
                <Divider />
                <Paper style={styles.insideCalendar} elevation={2}>
                    <TableContainer>
                        <Table>
                            {tableHead}
                            {getBody()}
                        </Table>
                    </TableContainer>
                </Paper>
                <Divider />
                <Paper style={styles.buttonGroup} elevation={0}>
                    <ButtonGroupComponent />
                </Paper>
            </Stack>

            <TableContainer component={Paper}>
                <Table sx={{ backgroundColor: '#D3F0F2' }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">Start Date</TableCell>
                            <TableCell align="center">End Date</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row: any) => (
                            <TableRow
                                key={row.name}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell align="center">{row.startDate}</TableCell>
                                <TableCell align="center">{row.endDate}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>

    );
}

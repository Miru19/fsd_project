import { AxiosError } from "axios";
import axiosInstance from "../instance";
const scheduleService = {
    async getSchedule(): Promise<any> {
        try {
            const resp = await axiosInstance.get("https://us-central1-swift-reef-334312.cloudfunctions.net/getSchedule");

            return resp.data;
        } catch (error) {
            const err = error as AxiosError
            if (err.response) {
                console.log(err.response.status)
                console.log(err.response.data)
            }
            throw error;
        }
    },
    async createSchedule(startDate: string, endDate: string) {
        try {
            const resp = await axiosInstance.get("https://us-central1-swift-reef-334312.cloudfunctions.net/createSchedule",
                {
                    params: {
                        "start_date": startDate + ".11.2021",
                        "end_date": endDate + ".11.2021"
                    }
                });
            return resp.data;
        } catch (error) {
            const err = error as AxiosError
            if (err.response) {
                console.log(err.response.status)
                console.log(err.response.data)
            }
            throw error;
        }
    }
}


export default scheduleService

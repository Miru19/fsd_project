import './App.css';
import { useState } from 'react';
import LoginPage from './Pages/LoginPage';
import TablePage from './Pages/TablePage';
import ImagePage from './Pages/ImagePage'
import AuthContext from './Pages/AuthContext'
import DatePicker from './components/DatePicker';
import { createBrowserHistory } from 'history';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import interceptors from './api/interceptors';



function App() {
  const history = createBrowserHistory();

  const [isAuth, setAuth] = useState(false);

  interceptors.setupInterceptors(history);

  return (

    <AuthContext.Provider value={isAuth}>
      <Router>
        <Switch>
          <Route
            exact path="/" render={(props) => ( <LoginPage {...props} setAuth={setAuth} />)} />
          <Route path="/table" component={TablePage} />
          <Route path="/upload" component={ImagePage} />
          <Route path="/schedule" component={DatePicker} />
        </Switch>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
